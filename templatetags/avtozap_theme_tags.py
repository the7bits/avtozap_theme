# -*- coding: utf-8 -*-

import math

from django.utils.translation import ugettext_lazy as _
from django import template

import lfs

register = template.Library()


@register.inclusion_tag('lfs/avtozap_theme_tags/avtozap_price.html', takes_context=True)
def avtozap_format_as_price(context, value):
    try:
        request = context['request']
    except:
        request = None
    shop = lfs.core.utils.get_default_shop(request)
    currency = shop.default_currency.abbr
    
    whole_part = math.trunc(value)

    fractional_part = math.modf(value)[0]
    fractional_part = math.trunc(round(fractional_part, 2)*100)
    if fractional_part == 0:
        fractional_part = '00'

    return {
        'whole_part': whole_part,
        'fractional_part': fractional_part,
        'currency': currency,
    }
